#!/bin/bash

set -e

# Source files
SCRIPT_ROOT="$(dirname $(realpath "$0") )"
SPK_PROT_ORIG_PATH="$SCRIPT_ROOT/cs35l41-dsp1-spk-prot.bin.orig"
SPK_PROT_MOD_PATH="$SCRIPT_ROOT/cs35l41-dsp1-spk-prot.bin.mod"

# Target files
FIRMWARE_ROOT="/usr/lib/firmware"
SPK_PROT_PATH="$FIRMWARE_ROOT/cs35l41-dsp1-spk-prot.bin"
STATUS_FILE="$FIRMWARE_ROOT/amp-control-status.txt"

# Options
SPK_PROT_COPY_FROM_PATH=
COMMAND=

function do_help()
{
    echo "Usage jupiter-amp-control <command>"
    echo
    echo "commands:"
    echo "   --legacy      Setup legacy test firmware"
    echo "   --restore     Restore original firmware"
    echo "   --status      Prints original/legacy"

    exit 0
}

while [[ $# -gt 0 ]]; do
    case "$1" in
        --legacy) COMMAND="update"; SPK_PROT_COPY_FROM_PATH="$SPK_PROT_MOD_PATH"; shift ;;
        --restore) COMMAND="update"; SPK_PROT_COPY_FROM_PATH="$SPK_PROT_ORIG_PATH"; shift ;;
        --status) COMMAND="status"; shift ;;
        *) echo "Unknown option $1"; do_help; exit 22;;
    esac
done

function OnExit()
{
    # Restore readonly state if necessary
    if [[ ! -z "$READONLY_STATUS" ]]; then
        if [[ "$READONLY_STATUS" == "enabled" ]]; then
            echo "Restoring read-only mode to: $READONLY_STATUS"
            steamos-readonly enable
        fi
    fi
}
trap OnExit EXIT

function do_status()
{
    if [[ ! -r "$STATUS_FILE" ]]; then
        echo "original"
        return
    fi

    cat "$STATUS_FILE"
    exit 0
}

function do_update()
{
    if [[ -z "$SPK_PROT_COPY_FROM_PATH" ]]; then
        echo "One of --legacy or --restore must be specified"
        exit 1
    fi

    NEW_STATUS=
    if [[ "$SPK_PROT_COPY_FROM_PATH" == "$SPK_PROT_ORIG_PATH" ]]; then
        NEW_STATUS="original"
    else
        NEW_STATUS="legacy"
    fi

    # Ensure the rootfs is writeable
    READONLY_STATUS="$(steamos-readonly status || true)"
    if [[ "$READONLY_STATUS" == "enabled" ]]; then
        echo "Disabling read-only mode. Previous mode: $READONLY_STATUS"
        steamos-readonly disable
    fi

    # Swap the current file for the desired file
    echo "Updating amp firmware:"
    echo "Source: '$SPK_PROT_COPY_FROM_PATH'"
    echo "Target: '$SPK_PROT_PATH'"
    cp "$SPK_PROT_COPY_FROM_PATH" "$SPK_PROT_PATH"
    echo "$NEW_STATUS" > "$STATUS_FILE"
    sync "$FIRMWARE_ROOT"

    # visual confirmation
    echo
    echo "Updated firmware md5sums:"
    md5sum "$SPK_PROT_PATH" "$SPK_PROT_ORIG_PATH" "$SPK_PROT_MOD_PATH"

    # TODO(andresr): maybe we can reload the driver on the fly?
    echo
    echo "Reboot system for changes to take effect"

    exit 0
}

case "$COMMAND" in
    update) do_update; shift ;;
    status) do_status; shift ;;
    *) echo "No command specified"; do_help; exit 22;;
esac
